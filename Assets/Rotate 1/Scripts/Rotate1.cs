using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class Rotate1 : MonoBehaviour
{
    private Camera _cam;
    private Rigidbody _rb;

    private RaycastHit hit = new RaycastHit();
    private Vector3 _startPoint = new Vector3();
    private float _startAngle;
    private bool _isNotInQuad = true;



    [SerializeField] private LayerMask _mapLayer;


    void Start()
    {
        _cam = Camera.main;
        _rb = GetComponent<Rigidbody>();


    }

    void Update()
    {
        Vector3 mouseClickPoint = Input.mousePosition;
        Ray ray = _cam.ScreenPointToRay(mouseClickPoint);

        if (Input.GetMouseButtonDown(0) || _isNotInQuad)
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, _mapLayer))
            {
                Vector3 targetDir = transform.position - hit.point;
                Vector3 forward = transform.forward;

                _startAngle = Vector3.SignedAngle(targetDir, forward, Vector3.right);
                _isNotInQuad = false;
            }
        }
        else
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, _mapLayer))
            {
                Vector3 targetDir = transform.position - hit.point;
                Vector3 forward = transform.forward;

                float angle = Vector3.SignedAngle(targetDir, forward, Vector3.right) - _startAngle;
                angle = angle % 360;
                transform.Rotate(0, angle, 0);
                _startPoint = transform.position - hit.point;

            }
            else
            {
                _isNotInQuad = true;
            }
        }
    }
}

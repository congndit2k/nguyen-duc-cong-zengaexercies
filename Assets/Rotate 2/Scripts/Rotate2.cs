using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Collections;
using UnityEngine;

public class Rotate2 : MonoBehaviour
{
    private Camera _cam;

    private Vector3 _previousPos;
    private float _startAngle;

    float minRotation = -.5f;
    float maxRotation = .5f;

    void Start()
    {
        _cam = Camera.main;
    }

    void Update()
    {

        Vector3 mouseClickPoint = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            _previousPos = transform.position - _cam.ScreenToWorldPoint(new Vector3(mouseClickPoint.x, mouseClickPoint.y, _cam.nearClipPlane));

            Vector3 forward = transform.forward;
            _startAngle = Vector3.SignedAngle(_previousPos, forward, Vector3.up) % 360;

        }
        else
        if (Input.GetMouseButton(0))
        {
            Vector3 curPos = transform.position - _cam.ScreenToWorldPoint(new Vector3(mouseClickPoint.x, mouseClickPoint.y, _cam.nearClipPlane));
             
            Vector3 forward = transform.forward;
            float angle = Vector3.SignedAngle(curPos, forward, Vector3.up);
            
            transform.Rotate(Vector3.up, angle - _startAngle);
            
            Quaternion currentRotation = transform.localRotation;
            currentRotation.y = Mathf.Clamp(currentRotation.y, minRotation, maxRotation);
            transform.localRotation = currentRotation;

            _startAngle = angle;
            _previousPos = _cam.ScreenToWorldPoint(new Vector3(mouseClickPoint.x, mouseClickPoint.y, _cam.nearClipPlane));
        }

    }
}


//float minRotation = -45;
//float maxRotation = 45;
//Vector3 currentRotation = transform.localRotation.eulerAngles;
//currentRotation.y = Mathf.Clamp(currentRotation.y, minRotation, maxRotation);
//transform.localRotation = Quaternion.Euler(currentRotation);

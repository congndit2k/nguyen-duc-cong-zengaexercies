using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : Work
{
    private Rigidbody _rb;


    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }
    public override void doSomeThing(RaycastHit hit)
    {
        Vector3 moveTo = new Vector3(hit.point.x, transform.position.y, hit.point.z);
        _rb.MovePosition(moveTo);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetMousePosition : MonoBehaviour
{

    private Camera _cam;
    private Rigidbody _rb;

    private RaycastHit[] hits = new RaycastHit[1];
    private RaycastHit hit = new RaycastHit();

    [SerializeField] private LayerMask _mapLayer;

    void Start()
    {
        _cam = Camera.main;
        _rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mouseClickPoint = Input.mousePosition;
            Ray ray = _cam.ScreenPointToRay(mouseClickPoint);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, _mapLayer))
            {
                Vector3 moveTo = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                _rb.MovePosition(moveTo);
            }
        }
    }

}

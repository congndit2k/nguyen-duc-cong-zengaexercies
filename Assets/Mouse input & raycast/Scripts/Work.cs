using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Work : MonoBehaviour
{
    public abstract void doSomeThing(RaycastHit hit);
}
